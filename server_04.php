<?php
 
ini_set("soap.wsdl_cache_enabled","0");
$server = new SoapServer("http://localhost:8080/aswlab041/ASWLabService.wsdl");

function FahrenheitToCelsius($fdegree){
    $cresult = ($fdegree - 32) * (5/9);
    return array("cresult"=> $cresult, "timeStamp"=> date('c', time()) );
}

function CurrencyConverter($from_Currency,$to_Currency,$amount) {
	$amount = urlencode($amount);
	$from_Currency = urlencode($from_Currency);
	$to_Currency = urlencode($to_Currency);
	$url = "http://www.google.com/ig/calculator?hl=en&q=$amount$from_Currency=?$to_Currency";
	$rawdata = file_get_contents($url);
	$data = explode('"', $rawdata);
	$error = $data['5'];
	if (!$error) {
		$data = explode(' ', $data['3']);
		$var = $data['0'];
		return round($var,3);
	}
	else return new SoapFault("Server", "Error message: ".$error);
};

// Tasques #4 i #5: Implementar la funci� GetHomeTowns i afegir-la al $server 

$server->addFunction("FahrenheitToCelsius");
// Tasca #3: $server->addFunction("CurrencyConverter");

$server->handle();
 
?>
